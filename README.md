# 🗃️ Datenstrukturen und Algorithmen


## "What is this?"

These are the files I used to create my cheat sheet for the "Datenstrukturen & Algorithmen" exam.

## "Can I just print it out and use it for my exam?"

Not directly. Use it as an inspiration and feel free to use some of the .tex lines I've written, but make it your own by extending and rewriting it. Some of the information on it might be wrong or outdated and the subjects covered in the course have shifted since then.

---

### **I take *no* responsibility if you fail using a printout of this cheat sheet.**